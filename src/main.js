import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import './plugins/highglight';
import './plugins/reveal';

Vue.config.productionTip = false;

new Vue({
  render(h) {
    return h(App);
  },
}).$mount('#app');
