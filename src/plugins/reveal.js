import Vue from 'vue';
import Reveal from 'reveal.js';
import 'reveal.js/css/reveal.css';
import 'reveal.js/css/theme/black.css';

let initialized = false;

Vue.use({
  install(vue) {
    vue.mixin({
      mounted() {
        if (!initialized) {
          Reveal.initialize({
            slideNumber: true,
          });
          initialized = true;
        }
      },
    });
  },
});
