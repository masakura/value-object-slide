import Vue from 'vue';
import VueHighlightJS from 'vue-highlight.js';
import 'highlight.js/styles/vs2015.css';

Vue.use(VueHighlightJS);
